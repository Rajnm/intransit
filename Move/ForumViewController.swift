//
//  ForumViewController.swift
//  Move
//
//  Created by Linus on 21/2/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

protocol FetchNameProtocol {
    func setForumName(_ newName: String)
}

class ForumViewController: UIViewController, FetchNameProtocol {
    
    var name = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadUser()
        // Do any additional setup after loading the view.
    }
    
    func loadUser(){
        guard let uid = FIRAuth.auth()?.currentUser?.uid else{
            return
        }
        let userRef = FIRDatabase.database().reference().child(Constants.USERS).child(uid)
        userRef.observeSingleEvent(of: .value, with: { (snapshot) in
            
            let dictionary = snapshot.value as! [String: Any]
            
            var user = User()
            
            user.setValuesForKeys(dictionary)
            
            self.setForumName(user.fullname!)
            
        })
    }
    
    @IBAction func loginMyselfPressed(_ sender: Any) {
        let currentUser = FIRAuth.auth()!.currentUser!
        performSegue(withIdentifier: "loginForumSegue" , sender: currentUser)
    }
    
    @IBAction func loginAnonPressed(_ sender: Any) {
        performSegue(withIdentifier: "loginForumSegue" , sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "loginForumSegue" {
            let vc = segue.destination as! TopicsViewController
            if sender == nil {
                vc.senderDisplayName = ""
            }else{
                vc.senderDisplayName = self.name
            }
        }
    }
    
    func setForumName(_ newName: String){
        self.name = newName
    }
}
