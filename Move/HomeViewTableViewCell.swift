//
//  HomeViewTableViewCell.swift
//  Move
//
//  Created by Raj karthik Nalini mohan on 9/2/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//

import UIKit

open class HomeViewTableViewCell: UITableViewCell {

    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var shout: UITextView!
    @IBOutlet weak var timeStampLabel: UILabel!
    
    @IBOutlet weak var commentsCount: UIButton!
    
     var tapAction: ((UITableViewCell) -> Void)?
    override open func awakeFromNib() {

        super.awakeFromNib()
        // Initialization code

     
    }
    
    
    open func configure(_ profilePic:String,name:String,shout:String,timestamp: String)
    {
        self.shout.allowsEditingTextAttributes = false

        self.shout.text = shout
        self.name.text = name
        let imageData = try? Data(contentsOf: URL(string:profilePic)!)
        self.profilePic.image = UIImage(data:imageData!)
        timeStampLabel.text = timestamp
      
    }
    
    
    
    @IBAction func CommentsButtonPressed(_ sender: Any) {
        tapAction?(self)
    }
    
    
    
    
    
    
}
