//
//  pdfTableViewController.swift
//  Move
//
//  Created by Raj karthik Nalini mohan on 18/4/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//

import UIKit
import Firebase

class pdfTableViewController: UITableViewController {
let storageRef = FIRStorage.storage().reference()
    var lessonArr = [String]()
    var pdfArr = [String]()
    override func viewDidLoad() {
        lessonArr = ["lesson 1","lesson 2","lesson 3","lesson 4"]
        pdfArr = ["https://firebasestorage.googleapis.com/v0/b/intransit-ios.appspot.com/o/pdf%2FAnkylosing_spondylitis.pdf?alt=media&token=ce5ad9f5-bbc8-490f-89ae-004f0a9645a7","https://firebasestorage.googleapis.com/v0/b/intransit-ios.appspot.com/o/pdf%2FArthritis.pdf?alt=media&token=1f187abf-e8a9-44fd-9f22-4160d8c7d51e","https://firebasestorage.googleapis.com/v0/b/intransit-ios.appspot.com/o/pdf%2FBack-pain.pdf?alt=media&token=d0b79744-f582-4c37-94d6-05846c0c8e22","https://firebasestorage.googleapis.com/v0/b/intransit-ios.appspot.com/o/pdf%2FBakers_cyst.pdf?alt=media&token=16672f49-4e57-42d4-89ab-62128973c60c"]
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return lessonArr.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pdfViewCell", for: indexPath)

  let mylabel = cell.viewWithTag(100) as! UILabel
        mylabel.text  = lessonArr[indexPath.row]

        return cell
    }
    
  override  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pdfname = pdfArr[indexPath.row]
        performSegue(withIdentifier: "pdf", sender: nil)
    }
    
   /* @IBAction func back(segue: UIStoryboardSegue) {
        
    } */


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
