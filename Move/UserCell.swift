//
//  UserCell.swift
//  Chat
//
//  Created by Linus on 9/2/17.
//  Copyright © 2017 Move. All rights reserved.
//

import UIKit
import Firebase

class UserCell: UITableViewCell {
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func convertTimestamp(seconds: NSNumber) -> String{
        let secs = seconds.doubleValue
        let timestampDate = Date(timeIntervalSince1970: secs)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm:ss a"
        let result = dateFormatter.string(from: timestampDate)
        
        return result
    }
    
    func getUser(forId: String) -> User{
        var user = User()
        let userRef = FIRDatabase.database().reference().child(Constants.USERS)
        userRef.child(forId).observeSingleEvent(of: .value, with: { (snapshot) in
            let dictionary = snapshot.value as? [String: Any]
            user.setValuesForKeys(dictionary!)
            print(user.fullname!)
        })
        return user
    }

}
