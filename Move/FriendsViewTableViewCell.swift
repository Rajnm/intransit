//
//  FriendsViewTableViewCell.swift
//  Move
//
//  Created by Raj karthik Nalini mohan on 17/2/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//

import UIKit

open class FriendsViewTableViewCell: UITableViewCell {
    @IBOutlet weak var profilePic: UIImageView!

    @IBOutlet weak var name: UILabel!

    @IBOutlet weak var shout: UITextView!
    @IBOutlet weak var commentsCount: UIButton!
    @IBOutlet weak var timeStampLabel: UILabel!
    var tapAction: ((UITableViewCell) -> Void)?
    override open func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    open func configure(_ profilePic:String,name:String,shout:String,timestamp: String)
    {
        self.shout.allowsEditingTextAttributes = false
        self.shout.text = shout
        self.name.text = name
        let imageData = try? Data(contentsOf: URL(string:profilePic)!)
        self.profilePic.image = UIImage(data:imageData!)
        timeStampLabel.text = timestamp
        
    }
    @IBAction func commentsButtonPressed(_ sender: Any) {
        tapAction?(self)
    }

}
