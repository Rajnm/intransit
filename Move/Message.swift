//
//  Message.swift
//  Chat
//
//  Created by Linus on 9/2/17.
//  Copyright © 2017 Move. All rights reserved.
//

import Foundation
import Firebase

class Message: NSObject {
    var fromId: String?
    var text: String?
    var timestamp: NSNumber?
    var toId: String?
    
    func chatPartnerId() -> String?{
        //one liner
        //return fromId == FIRAuth.auth()?.currentUser.uid ? toId : fromId
        if fromId == FIRAuth.auth()?.currentUser?.uid{
            return toId
        }else{
            return fromId
        }
    }
    
}
