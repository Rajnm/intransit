//
//  NewMessageViewController.swift
//  Chat
//
//  Created by Linus on 9/2/17.
//  Copyright © 2017 Move. All rights reserved.
//

import UIKit
import Firebase

class NewMessageViewController: UITableViewController {
    
    var messagesViewController: MessagesViewController?
    
    var users =  [User]()
    let cellId = "userCell2"
    override func viewDidLoad() {
        super.viewDidLoad()
        observeUsers()
    }
    
    func observeUsers(){
        FIRDatabase.database().reference().child(Constants.USERS).observe(.childAdded, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let user = User()
                user.uid = snapshot.key
                
                //this crashes if dictionary in db does not match class
                user.setValuesForKeys(dictionary)
                //safer
                //user.name = dictionary["name"]
                if user.uid != FIRAuth.auth()?.currentUser?.uid{
                    self.users.append(user)
                }
                
                //this crashes due to bg thread needs dispatch async
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        })
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! UserCell
        let user = users[indexPath.row]
        
        cell.titleLabel?.text = user.fullname
        cell.subTitleLabel?.text = user.email
        cell.timestampLabel?.text = ""
        
        if let profileImageUrl = user.urlToImage {
            cell.profilePicture?.loadImageUsingCacheWithUrlString(profileImageUrl)
            cell.profilePicture?.layer.cornerRadius = 24
            cell.profilePicture?.layer.masksToBounds = true
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true, completion: nil)
        self.messagesViewController?.showChatLogFor(toId: users[indexPath.row].uid!)
        
    }
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
