//
//  FriendsCommentViewController.swift
//  Move
//
//  Created by Raj karthik Nalini mohan on 22/2/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//

import UIKit
import Firebase

class FriendsCommentViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate {
    
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var commentsTableView: UITableView!
    
    @IBOutlet weak var addCommentText: UITextView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    
    var shoutUserId: String?
    var shoutid: String?
    var comments = [NSDictionary]()
    var databaseRef = FIRDatabase.database().reference()
    var loggedInUser:AnyObject?
    
    var viewThisUser: String?
    var viewThisUserData: NSDictionary?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loggedInUser = FIRAuth.auth()?.currentUser
        addCommentText.delegate = self
        
        
        addCommentText.textContainerInset = UIEdgeInsetsMake(30, 20, 20, 20)
        addCommentText.text = "Write a comment...."
        addCommentText.textColor = UIColor.lightGray
        
        
        
        //observe comments for this shout
        
        self.databaseRef.child("comments").child(shoutUserId!).child(shoutid!).observe(.childAdded, with: { (snapshot:FIRDataSnapshot) in
            
            self.comments.append(snapshot.value as! NSDictionary)
            self.addCommentToShout()
            self.commentsTableView.insertRows(at: [IndexPath(row:0,section:0)], with: UITableViewRowAnimation.automatic)
            
        }){(error) in
            
            //print(error)
        }
        
        
        let userRef = FIRDatabase.database().reference().child(Constants.USERS).child((loggedInUser?.uid)!)
        
        userRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String : Any]{
                let user = User()
                user.setValuesForKeys(dictionary)
                
                self.name.text = user.fullname
                
                let imageUrl = user.urlToImage
                self.storageRef.reference(forURL: imageUrl!).data(withMaxSize: 1 * 1024 * 1024, completion: { (imgData, error) in
                    if error == nil {
                        if let data = imgData {
                            self.profilePic.image = UIImage(data: data)
                        }
                    } else {
                        print(error!.localizedDescription)
                    }
                })
            }
        })
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func textFieldShouldReturn(_ textField: UITextView) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    var storageRef: FIRStorage {
        return FIRStorage.storage()
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (addCommentText.textColor == UIColor.lightGray)
        {
            addCommentText.text = nil
            addCommentText.textColor = UIColor.black
        }
    }
    
    @IBAction func postButtonPressed(_ sender: Any) {
        let commentsRef = databaseRef.child("comments")
        let userCommentsRef = commentsRef.child(shoutUserId!)
        let shoutUserCommentsRef = userCommentsRef.child(shoutid!)
        
        
        let commentid = shoutUserCommentsRef.childByAutoId().key
        if (addCommentText.text.characters.count>0)
        {
            
            let dateInSeconds = Date().timeIntervalSince1970
            let date = Date(timeIntervalSince1970: dateInSeconds)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd hh:mm a"
            
            let stamp = "\(formatter.string(from: date))"
            
            let values: [String: Any] = ["text": addCommentText.text,
                                         "timestamp": stamp,
                                         "commentid": commentid,
                                         "userid": loggedInUser?.uid]
            
            
            shoutUserCommentsRef.child(commentid).updateChildValues(values)
        }
        
        addCommentText.text = nil
        
    }
    
    
    func addCommentToShout(){
        //create a ref to the shout u want to update
        var countsref =  databaseRef.child("shouts").child("userid").child("shoutid").child("counter")
        
        
        var counts = self.comments.count
        self.databaseRef.child("shouts").child(shoutUserId!).child(shoutid!).updateChildValues(["counter":counts])
        
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell: FriendsCommentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FriendsCommentCell", for: indexPath) as! FriendsCommentTableViewCell
        let currentComment = comments[(self.comments.count-1) - (indexPath.row)]
        
        cell.getUserDataForUserId(id: currentComment["userid"] as! String)
        
        
        cell.configure(currentComment["text"] as! String,timestamp: currentComment["timestamp"] as! String)
        return cell
        
    }
    
}



