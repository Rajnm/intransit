//
//  PostViewController.swift
//  Forum
//
//  Created by Linus on 16/2/17.
//  Copyright © 2017 Move. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import Firebase

class PostViewController: JSQMessagesViewController{
    var messages = [JSQMessage]()
    var avatars = Dictionary<String, UIImage>()
    
    lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
    lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()
    let postsRef = FIRDatabase.database().reference().child(Constants.POSTS)
    let repliesRef = FIRDatabase.database().reference().child(Constants.REPLIES)
    
    var post: Post? {
        didSet {
            title = post?.title
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        senderId = FIRAuth.auth()?.currentUser?.uid
        
        // No avatars
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        
        observeReplies()
    }
    
    private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleGreen())
    }
    
    private func setupIncomingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        
        if message.senderId == senderId {
            return outgoingBubbleImageView
        }else{
            return incomingBubbleImageView
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        let message = messages[indexPath.item]
        
        if message.senderId == senderId {
            cell.textView?.textColor = UIColor.white
        } else {
            cell.textView?.textColor = UIColor.black
        }
        return cell
    }
    
    // View  usernames above bubbles
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let message = messages[indexPath.item]
        
        if message.senderId == senderId {
            return nil
        }
        
        if indexPath.item > 0{
            let prevMessage = messages[indexPath.item-1]
            if prevMessage.senderId == message.senderId && prevMessage.senderDisplayName == message.senderDisplayName {
                return nil
            }
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yy hh:mm a"
        let dateString = dateFormatter.string(from: message.date)
        
        var result = message.senderDisplayName + " " + dateString
        
        return NSAttributedString(string: result)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        let message = messages[indexPath.item]
        
        //Sent by me, skip
        if message.senderId == senderId {
            return CGFloat(0.0)
        }
        
        //Same as previous sender, skip
        if indexPath.item > 0 {
            let previousMessage = messages[indexPath.item-1]
            if previousMessage.senderId == message.senderId && previousMessage.senderDisplayName == message.senderDisplayName {
                return CGFloat(0.0)
            }
        }
        
        return kJSQMessagesCollectionViewCellLabelHeightDefault
    }
    
    
    private func addMessage(withId id: String, name: String, text: String, date: Date) {
        if let message = JSQMessage(senderId: id, senderDisplayName: name, date: date, text: text){
            messages.append(message)
        }
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        let newReplyForThisPostRef = repliesRef.child((post?.postId)!)
        let timestamp = NSNumber(value: Int(Date().timeIntervalSince1970))
        let values: [String: Any] = [Constants.SENDER_ID: senderId,
                                     Constants.SENDER_DISPLAY_NAME: senderDisplayName,
                                     Constants.TEXT: text,
                                     Constants.TIMESTAMP: timestamp]
        newReplyForThisPostRef.childByAutoId().updateChildValues(values)
        
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        finishSendingMessage()
    }
    
    override func didPressAccessoryButton(_ sender: UIButton!) {
        return
    }
    
    
    
    func observeReplies(){
        guard let postId = post?.postId else{
            return
        }
        repliesRef.child(postId).observe(.childAdded, with: { (snapshot) in
            let dictionary = snapshot.value as! [String:Any]
            var reply = Reply()
            reply.setValuesForKeys(dictionary)
            //back to date
            let timestampDate = Date(timeIntervalSince1970: (reply.timestamp?.doubleValue)!)
            self.addMessage(withId: reply.senderId!, name: reply.senderDisplayName!, text: reply.text!, date: timestampDate)
            self.finishReceivingMessage()
        })
    }
}
