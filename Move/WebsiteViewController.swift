//
//  WebsiteViewController.swift
//  Move
//
//  Created by Raj karthik Nalini mohan on 17/2/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//

import UIKit

class WebsiteViewController: UIViewController {
   
    @IBOutlet weak var navigationTitle: UINavigationItem!
    @IBOutlet weak var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        //let url = NSURL(string: "http://www.move.org.au/Home")!

       let url = NSURL(string: "http://www.move.org.au/Useful-Information/Information-to-Download")!
  
        let request = NSURLRequest(url: url as URL)
        webView.loadRequest(request as URLRequest)
       webViewDidFinishLoad(webView: webView)
      
    }
    func webViewDidFinishLoad(webView: UIWebView) {
       webView.frame.size.height = 1
       webView.frame.size = webView.sizeThatFits(CGSize.zero)
        
        webView.scalesPageToFit = true
        webView.isUserInteractionEnabled = true
        webView.scrollView.isScrollEnabled = true
        
     
    }

   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
      
    }
    
    @IBAction func backAction(_ sender: Any) {
        if webView.canGoBack {
            webView.goBack()
        }
    }
    @IBAction func forwardAction(_ sender: Any) {
        if webView.canGoForward {
            webView.goForward()
        }

    }

    @IBAction func stopAction(_ sender: Any) {
        webView.stopLoading()
    }
    @IBAction func refreshAction(_ sender: Any) {
         webView.reload()
    }

    @IBAction func homeAction(_ sender: Any) {
        viewDidLoad()
     
        
//        let url = NSURL(string: "http://www.move.org.au/Useful-Information/Information-to-Download")!
//        
//        let request = NSURLRequest(url: url as URL)
//        webView.loadRequest(request as URLRequest)
//        webViewDidFinishLoad(webView: webView)
        
    }

}
