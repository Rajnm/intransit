//
//  TopicViewController.swift
//  Forum
//
//  Created by Linus on 16/2/17.
//  Copyright © 2017 Move. All rights reserved.
//

import UIKit
import Firebase


class TopicsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate{
    
    @IBOutlet weak var topicsTable: UITableView!
    @IBOutlet weak var searchTopicsBar: UISearchBar!
    
    var filteredTopics = [Topic]()
    var topics = [Topic]()
    var topicsList = ["Health", "Questions", "Relationships", "Lifestyle", "Sports"]
    var senderDisplayName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topicsTable.dataSource = self
        searchTopicsBar.delegate = self
        //filteredTopics = topics
        
        //loadTopicsIntoDatabase()
        observeTopics()
    }
    
    func observeTopics(){
        let topicsRef = FIRDatabase.database().reference().child(Constants.TOPICS)
        topicsRef.observe(.childAdded, with: { (snapshot) in
            var topic = Topic()
            let data = snapshot.value as! [String: Any]
            topic.topicId = data[Constants.TOPIC_ID] as? String
            topic.topicName = data[Constants.TOPIC_NAME] as? String
            //append topic to topics
            self.topics.append(topic)
            self.filteredTopics.append(topic)
            
            DispatchQueue.main.async {
                self.topicsTable.reloadData()
            }
        })
    }
    
    func loadTopicsIntoDatabase(){
        for topic in topicsList {
            let topicsRef = FIRDatabase.database().reference().child(Constants.TOPICS)
            topicsRef.childByAutoId().observeSingleEvent(of: .value, with: { (snapshot) in
                let topicId = snapshot.key
                let values: [String: Any] = [Constants.TOPIC_NAME: topic,
                                             Constants.TOPIC_ID: topicId]
                topicsRef.child(topicId).updateChildValues(values, withCompletionBlock: { (error, ref) in
                    if error != nil {
                        print(error?.localizedDescription)
                    }else{
                        //child updated successfully
                    }
                })
            })
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        filteredTopics = searchText.isEmpty ? topics: topics.filter { (item: Topic) -> Bool in
            return item.topicName?.range(of: searchText, options: .caseInsensitive, range: nil, locale:nil) != nil
        }
        
        topicsTable.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredTopics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = topicsTable.dequeueReusableCell(withIdentifier: "topicCell", for: indexPath)
        
        let topic = filteredTopics[indexPath.row]
        
        cell.textLabel?.text = topic.topicName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let topic = topics[indexPath.row]
        self.performSegue(withIdentifier: "showTopicPosts", sender: topic)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showTopicPosts"{
            let topic = sender as! Topic
            let vc = segue.destination as! PostsViewController
            vc.topicId = topic.topicId
            vc.topicTitle = topic.topicName
            vc.senderDisplayName = self.senderDisplayName
        }
    }
    
    
    
    @IBAction func addTopicPressed(_ sender: Any) {
        // Alert View for input
        let alert = UIAlertController(title: "Forum Topic", message: "Add an Topic", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save", style: .default) { (action: UIAlertAction!) -> Void in
            // Get the text field for the forum title
            let topicTextField = alert.textFields![0]
            
            // Get new topic name
            let topicName = topicTextField.text!
            
            let topicsRef = FIRDatabase.database().reference().child(Constants.TOPICS)
            let newTopicRef = topicsRef.childByAutoId()
            
            //Get topic Id
            let newTopicId = newTopicRef.key
            //add topic to database
            newTopicRef.updateChildValues([Constants.TOPIC_ID: newTopicId,
                                           Constants.TOPIC_NAME: topicName])
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .default) { (action: UIAlertAction!) -> Void in
        }
        
        alert.addTextField { (topicTextField: UITextField) -> Void in
        }
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        //alert.view.tintColor = UIColor.orange
        
        present(alert, animated: true, completion: nil)
    }
    

    @IBAction func backToTopicsPressed(segue: UIStoryboardSegue){
    }
    
}
