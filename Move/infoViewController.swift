//
//  infoViewController.swift
//  Move
//
//  Created by Raj karthik Nalini mohan on 14/2/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//

import UIKit
import Firebase


class infoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var pdfView: UILabel!
   
    let storageRef = FIRStorage.storage().reference()
    var pdfUrl = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pdfUrl = [
            "https://firebasestorage.googleapis.com/v0/b/intransit-ios.appspot.com/o/pdf%2FLiteracy_and_Numeracy_Test_for_Initial_Teacher_Education_students_-_Sample_Questions.pdf?alt=media&token=7b1776b0-d552-4412-8b20-0f067b21f81f",
            "https://firebasestorage.googleapis.com/v0/b/intransit-ios.appspot.com/o/pdf%2FMET_SampleTest_A.pdf?alt=media&token=52022ee8-f0e6-4cb1-89e1-d5c5ffe4bf0e",
            "https://firebasestorage.googleapis.com/v0/b/intransit-ios.appspot.com/o/pdf%2Fpdf-sample.pdf?alt=media&token=4a696abf-639d-490d-b695-5ad1e286d3d8",
            "https://firebasestorage.googleapis.com/v0/b/intransit-ios.appspot.com/o/pdf%2FSampleQuestions.pdf?alt=media&token=dc3a39de-df0b-47eb-be3a-09ecf30f90c7"]
       
    }
    
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
        }
    
   func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pdfUrl.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let mylabel = cell.viewWithTag(100) as! UILabel
        
        mylabel.text = pdfUrl[indexPath.row]
        
        
        //cell.textLabel?.text = "test"
        
        return cell
        
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pdfname = pdfUrl[indexPath.row]
        performSegue(withIdentifier: "pdf", sender: nil)
    }
    
    @IBAction func back(segue: UIStoryboardSegue) {
        
    }

}
