//
//  PostCell.swift
//  Forum
//
//  Created by Linus on 21/2/17.
//  Copyright © 2017 Move. All rights reserved.
//

import UIKit

class PostCell: UITableViewCell {
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var postAuthor: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        postAuthor.font = UIFont.boldSystemFont(ofSize: 12.0)
        postAuthor.textColor = UIColor.lightGray
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
