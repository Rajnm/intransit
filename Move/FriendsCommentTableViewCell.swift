//
//  FriendsCommentTableViewCell.swift
//  Move
//
//  Created by Raj karthik Nalini mohan on 22/2/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//

import UIKit
import Firebase

class FriendsCommentTableViewCell: UITableViewCell {
    @IBOutlet weak var profilePic: UIImageView!

    @IBOutlet weak var commentsView: UITextView!
    @IBOutlet weak var timeStamp: UILabel!
    @IBOutlet weak var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func getUserDataForUserId(id: String){
        //create ref
        
        let userRef = FIRDatabase.database().reference().child(Constants.USERS).child(id)
        let storageRef = FIRStorage.storage()
        //observe the current userid
        userRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String : Any]{
                
                //assign an object this data
                let user = User()
                user.setValuesForKeys(dictionary)
                self.name.text = user.fullname
                let imageUrl = user.urlToImage
                storageRef.reference(forURL: imageUrl!).data(withMaxSize: 1 * 1024 * 1024, completion: { (imgData, error) in
                    if error == nil {
                        if let data = imgData {
                            self.profilePic.image = UIImage(data: data)
                        }
                    } else {
                        print(error!.localizedDescription)
                    }
                    
                })
                
            }
        })
    }

 
    open func configure(_ comment: String,timestamp: String)
    {
        self.commentsView.text = comment
        
        self.timeStamp.text = timestamp
        
    }
}
