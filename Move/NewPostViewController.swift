//
//  NewPostViewController.swift
//  Forum
//
//  Created by Linus on 16/2/17.
//  Copyright © 2017 Move. All rights reserved.
//

import UIKit
import Firebase

class NewPostViewController: UIViewController, UITextViewDelegate{
    @IBOutlet weak var titleTextView: UITextView!
    @IBOutlet weak var subjectTextView: UITextView!
    var senderDisplayName: String?
    var topicId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleTextView.delegate = self
        
        titleTextView.textColor = UIColor.lightGray
        subjectTextView.textColor = UIColor.lightGray
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (titleTextView.textColor == UIColor.lightGray){
        titleTextView.text = nil
        subjectTextView.text = nil
        titleTextView.textColor = UIColor.black
        subjectTextView.textColor = UIColor.black
        }

    }
    
    var postsViewController: PostsViewController?
    
    @IBAction func createNewPostPressed(_ sender: Any) {
        //create reference to posts
        let postsForTopicRef = FIRDatabase.database().reference().child(Constants.POSTS).child(topicId!)
    
        guard let currentUserId = FIRAuth.auth()?.currentUser?.uid else{
            return
        }
        
        //save post to database
        //create new postid
        
        postsForTopicRef.childByAutoId().observeSingleEvent(of: .value, with: { (snapshot) in
            
            let newPostId = snapshot.key
            let newPostRef = postsForTopicRef.child(newPostId)
            newPostRef.observeSingleEvent(of: .value, with: { (snapshot2) in
                let timestamp = NSNumber(value: Int(Date().timeIntervalSince1970))
                let values: [String: Any] = [Constants.SENDER_ID: currentUserId,
                                             Constants.SENDER_DISPLAY_NAME: self.senderDisplayName!,
                                             Constants.TIMESTAMP: timestamp,
                                             Constants.TITLE: self.titleTextView.text,
                                             Constants.SUBJECT: self.subjectTextView.text,
                                             Constants.POST_ID: newPostId,
                                             Constants.TOPIC_ID: self.topicId!]
                
                //create post
                newPostRef.updateChildValues(values)
            })
            
            //create a new "Reply" reference which contains all replies to this post under this newPostId
            //and set subject as the first response
            let newReplyRef = FIRDatabase.database().reference().child(
                Constants.REPLIES).child(newPostId)
        
            let timestamp = NSNumber(value: Int(Date().timeIntervalSince1970))
            let replyValues: [String: Any] = [Constants.SENDER_ID: currentUserId,
                                              Constants.SENDER_DISPLAY_NAME: self.senderDisplayName!,
                                              Constants.TEXT: self.subjectTextView.text,
                                              Constants.TIMESTAMP: timestamp]
            //create reply thread
            newReplyRef.childByAutoId().updateChildValues(replyValues, withCompletionBlock: { (error, ref) in
                if error != nil {
                    print("Error with adding new reply: ", error!)
                }else{
                    //transition to the post just created in PostViewController
                    self.dismiss(animated: true, completion: nil)
                    self.postsViewController?.showPostFor(postId: newPostId)
                }
            })
        })
    }
}
