//
//  Post.swift
//  Forum
//
//  Created by Linus on 16/2/17.
//  Copyright © 2017 Move. All rights reserved.
//

import Foundation

class Post: NSObject{
    var postId: String?
    var senderId: String?
    var senderDisplayName: String?
    var title: String?
    var subject: String?
    var timestamp: NSNumber?
    var topicId: String?
}


