//
//  SettingsViewController.swift
//  Move
//
//  Created by nuzz on 28/2/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func feedbackPressed(_ sender: Any) {
        performSegue(withIdentifier: "reportSegue", sender: "Feedback")
    }
    
    @IBAction func reportBugPressed(_ sender: Any) {
        performSegue(withIdentifier: "reportSegue", sender: "Bug")
    }
    
    @IBAction func reportSpamPressed(_ sender: Any) {
        performSegue(withIdentifier: "reportSegue", sender: "Spam")
    }
    
    @IBAction func reportAbusePressed(_ sender: Any) {
        performSegue(withIdentifier: "reportSegue", sender: "Abusive content")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "reportSegue" {
            let title = sender as! String
            let vc = segue.destination as! ReportViewController
            vc.titleName = title
        }
    }
}
