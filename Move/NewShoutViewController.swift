//
//  NewShoutViewController.swift
//  Move
//
//  Created by Raj karthik Nalini mohan on 8/2/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//

import UIKit
import Firebase

class NewShoutViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UINavigationControllerDelegate {
    
 
    @IBOutlet weak var newFeedTextView: UITextView!
    
    // Create reference to the database
   var databaseRef = FIRDatabase.database().reference()
    var loggedInUser:AnyObject?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.newFeedTextView.layer.borderWidth = 2.5
        self.newFeedTextView.layer.borderColor = UIColor.gray.cgColor
        self.newFeedTextView.layer.cornerRadius = 10
        self.newFeedTextView.layer.masksToBounds = true
        self.loggedInUser = FIRAuth.auth()?.currentUser
        
        newFeedTextView.delegate = self
        newFeedTextView.textContainerInset = UIEdgeInsetsMake(30, 20, 20, 20)
        newFeedTextView.text = "What's in your mind?"
        newFeedTextView.textColor = UIColor.lightGray
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func didTapCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (newFeedTextView.textColor == UIColor.lightGray)
        {
            newFeedTextView.text = nil
            newFeedTextView.textColor = UIColor.black
        }
    }
 
    
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    

    @IBAction func didTapShout(_ sender: Any) {
    
        
        let key = self.databaseRef.child("shouts").childByAutoId().key
        if (newFeedTextView.text.characters.count>0)
        {
            
            let dateInSeconds = Date().timeIntervalSince1970
            let date = Date(timeIntervalSince1970: dateInSeconds)
            
            let formatter = DateFormatter()
             formatter.dateFormat = "yyyy/MM/dd hh:mm a"
            
           let stamp = "\(formatter.string(from: date))"
            
            var counts = 0
            
            let childUpdates = ["/shouts/\(self.loggedInUser!.uid!)/\(key)/text":newFeedTextView.text,
                                "/shouts/\(self.loggedInUser!.uid!)/\(key)/timestamp":stamp, "/shouts/\(self.loggedInUser!.uid!)/\(key)/shoutid":key,
                                "/shouts/\(self.loggedInUser!.uid!)/\(key)/userid":loggedInUser?.uid,
                                "/shouts/\(self.loggedInUser!.uid!)/\(key)/counter":counts] as [String : Any]
            
            self.databaseRef.updateChildValues(childUpdates)
            
            dismiss(animated: true, completion: nil)
        }
        
    }
        
    
        
        
    
        
    
  
    
    }

   


