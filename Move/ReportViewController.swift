//
//  ReportViewController.swift
//  Move
//
//  Created by nuzz on 28/2/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//

import UIKit
import MessageUI

class ReportViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    var titleName: String?
    
    @IBOutlet weak var reportTitleLabel: UILabel!
    @IBOutlet weak var emailTextView: UITextView!
    @IBOutlet weak var popupView: UIView!
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    let TO_EMAIL = ["dbiosapp@gmail.com"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reportTitleLabel?.text = titleName
        
        popupView.layer.cornerRadius = 10
        popupView.layer.masksToBounds = true
        
    }
    
    @IBAction func sendReportPressed(_ sender: Any) {
        if !MFMailComposeViewController.canSendMail(){
            alertTheUser(title: "Mail Error", message: "Mail services not avaliable")
            return
        }else{
            sendEmail(from: "InTransitApp@gmail.com" , to: TO_EMAIL, subject: titleName!, body: emailTextView.text)
        }
    }
    
    func sendEmail(from: String, to: [String], subject: String, body: String){
        print("From: \(from) \nTo: \(to)\n\nSubject: \(subject) \nBody: \(body)")
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        
        composeVC.setToRecipients(to)
        composeVC.setSubject(subject)
        composeVC.setMessageBody(body, isHTML: false)
        
        self.present(composeVC, animated: true, completion: nil)
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        
        dismiss(animated: true, completion: nil)
        
        
        
        
        self.alertTheUser(title: "Sent success", message: "\(result.rawValue)")
    }
    
    
}
