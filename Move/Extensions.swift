//
//  Extensions.swift
//  Move
//
//  Created by Linus on 10/2/17.
//  Copyright © 2017 Move. All rights reserved.

import UIKit

let imageCache = NSCache<NSString, UIImage>()

extension UIViewController {
    func alertTheUser(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
        let ok = UIAlertAction(title:"OK", style: .default, handler: nil)
        
        alert.addAction(ok)
        present(alert, animated: true, completion:nil)
    }
}

extension UIImageView {
    
    func loadImageUsingCacheWithUrlString(_ urlString: String){
        
        self.image = nil
        
        
        //check cache for image
        if let cachedImage = imageCache.object(forKey:urlString as NSString) {
            self.image = cachedImage
            return
        }
        
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            if error != nil{
                print(error!)
                return
            }
            
            DispatchQueue.main.async {
                if let downloadedImage = UIImage(data: data!){
                    imageCache.setObject(downloadedImage, forKey: urlString as NSString)
                    self.image = downloadedImage
                }
            }
            
        }).resume()
    }
}
