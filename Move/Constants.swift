//
//  Constants.swift
//  Move
//
//  Created by Linus on 1/2/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//

import Foundation

class Constants {
    
    static let USERS = "users"
    static let USER_MESSAGES = "user-messages"
    
    static let FROM_ID = "fromId"
    static let TO_ID = "toId"
    static let TIMESTAMP = "timestamp"
    
    static let USER_ID = "uid"
    static let FULL_NAME = "fullname"
    static let EMAIL = "email"
    static let PHONE = "phonenum"
    static let DOB = "dateofbirth"
    static let GENDER = "gender"
    static let CITY = "city"
    static let PROFILE_PICTURE_URL = "urlToImage"
    
    
    //DBProvider
    static let MESSAGES = "messages"
    static let MEDIA_MESSAGES = "Media_Messages"
    static let IMAGE_STORAGE = "Image_Storage"
    static let VIDEO_STORAGE = "Video_Storage"
    
    static let DATA = "data"
    
    //messages
    static let TEXT = "text"
    static let SENDER_NAME = "sender_name"
    static let URL = "url"
    
    //posts and replies
    
    static let POSTS = "posts"
    static let POST_ID = "postId"
    static let REPLIES = "replies"
    static let SENDER_ID = "senderId"
    static let SENDER_DISPLAY_NAME = "senderDisplayName"
    static let TITLE = "title"
    static let SUBJECT = "subject"
    
    //topics
    static let TOPICS = "topics"
    static let TOPIC_NAME = "topicname"
    static let TOPIC_ID = "topicId"
    
}
