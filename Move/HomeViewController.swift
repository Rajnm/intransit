//
//  HomeViewController.swift
//  Move
//
//  Created by Raj karthik Nalini mohan on 9/2/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//

import UIKit
import Firebase


class HomeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var aivLoading: UIActivityIndicatorView!
    
    
    var databaseRef = FIRDatabase.database().reference()
    //    var loggedInUser:AnyObject?
    //                var friend:AnyObject?
    //    var loggedInUserData:NSDictionary?
    //                var friendData:NSDictionary?
    
    var viewThisUser: String?
    var viewThisUserData: NSDictionary?
    
    var shouts = [NSDictionary]()
    var comment = [NSDictionary]()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.loggedInUser = FIRAuth.auth()?.currentUser
       
        //get the logged in users details
        self.databaseRef.child("users").child(self.viewThisUser!).observeSingleEvent(of: .value) { (snapshot:FIRDataSnapshot) in
            
            //store the logged in users details into the variable
            self.viewThisUserData = snapshot.value as? NSDictionary
            // print(self.loggedInUserData)
            
            //get all the tweets that are made by the user
            
            self.databaseRef.child("shouts").child(self.viewThisUser!).observe(.childAdded, with: { (snapshot:FIRDataSnapshot) in
                
                self.shouts.append(snapshot.value as! NSDictionary)
                
                self.homeTableView.insertRows(at: [IndexPath(row:0,section:0)], with: UITableViewRowAnimation.automatic)
                
                self.aivLoading.stopAnimating()
                
            }){(error) in
                
                //print(error)
            }
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.shouts.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell: HomeViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "HomeViewTableViewCell", for: indexPath) as! HomeViewTableViewCell
        
        let currentShout = shouts[(self.shouts.count-1) - (indexPath.row)]
        
        
        cell.configure(self.viewThisUserData!["urlToImage"] as! String,name:self.viewThisUserData!["fullname"] as! String,shout:currentShout["text"] as! String, timestamp: currentShout["timestamp"] as! String)
        
       let counte = currentShout["counter"] as! Int
        var counts = "Comments (\(counte))"
        cell.commentsCount.setTitle(counts, for: .normal)
        
    
   
        
        // Assign the tap action which will be executed when the user taps the UIButton
        cell.tapAction = { (cell) in
            //self.showAlertForRow(row: self.homeTableView.indexPath(for: cell)!.row)
            //move to comment vc
            let shoutThatWasTapped = self.shouts[(self.shouts.count-1) - (self.homeTableView.indexPath(for: cell)!.row)]
            
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "comments") as! CommentsViewController
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .coverVertical
            vc.shoutid = shoutThatWasTapped["shoutid"] as? String
            vc.shoutUserId = shoutThatWasTapped["userid"] as? String
            vc.isFromHome = true
            self.present(vc, animated: true, completion: nil)
            //once completed presents home
            
            // print("view comments")
            
        }
        
        return cell
    }
    
    @IBAction func backToHome(segue: UIStoryboardSegue){
        
    }
    
}
