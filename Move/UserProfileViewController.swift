//
//  userProfileViewController.swift
//  Move
//
//  Created by Raj karthik Nalini mohan on 2/2/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//


import UIKit
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage
import Firebase


class userProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var cityTxt: UITextField!
    
    
    @IBOutlet weak var phoneNumTxt: UITextField!
    
    let picker = UIImagePickerController()
    var userStorage:FIRStorageReference!
    var ref:FIRDatabaseReference!
    
    
    @IBAction func logout(_ sender: Any) {
        if FIRAuth.auth()!.currentUser != nil {
            try! FIRAuth.auth()!.signOut()
            
            if FIRAuth.auth()?.currentUser == nil {
                let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as! LoginViewController
                self.present(loginVC, animated:true, completion: nil)
                
            }
        }
        
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login")
        self.present(vc, animated: true, completion: nil)
        
    }
    @IBOutlet weak var fullNameTxt: UITextField!
    
    var dataBaseRef: FIRDatabaseReference!{
        return FIRDatabase.database().reference()
    }
    
    var storageRef: FIRStorage {
        return FIRStorage.storage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        profilePic.layer.cornerRadius = 65
        
        let storage = FIRStorage.storage().reference()
        ref = FIRDatabase.database().reference()
        userStorage = storage.child(Constants.USERS)
        
        var TapGesture = UITapGestureRecognizer(target: self, action: "ImageTapped")
        self.profilePic.addGestureRecognizer(TapGesture)
        
        loadUserInfo()
    }
    
    
    func ImageTapped() {
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        present(picker, animated: true, completion: nil)
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //self.profilePic.image = nil
        if let newImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.profilePic.image = newImage
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func updatePic(_ sender: Any) {
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        present(picker, animated: true, completion: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func loadUserInfo() {
        guard let id = FIRAuth.auth()?.currentUser?.uid else{
            return
        }
        
        let userRef = FIRDatabase.database().reference().child(Constants.USERS).child(id)
        
        userRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String : Any]{
                let user = User()
                user.setValuesForKeys(dictionary)
                
                self.fullNameTxt.text = user.fullname
                self.phoneNumTxt.text = user.phonenum
                self.cityTxt.text = user.city
                
                let imageUrl = user.urlToImage
                self.storageRef.reference(forURL: imageUrl!).data(withMaxSize: 1 * 1024 * 1024, completion: { (imgData, error) in
                    if error == nil {
                        if let data = imgData {
                            self.profilePic.image = UIImage(data: data)
                        }
                    } else {
                        print(error!.localizedDescription)
                        self.alertTheUser(title: "Loading Image Error", message: (error?.localizedDescription)!)
                    }
                })
            }
        })
    }
    
    @IBAction func update(_ sender: Any) {
        guard fullNameTxt.text != "", phoneNumTxt.text != "", cityTxt.text != "",  profilePic.image != nil else {
            return
        }
        guard let uid = FIRAuth.auth()?.currentUser?.uid else{
            return
        }
        
        let changeRequest = FIRAuth.auth()!.currentUser!.profileChangeRequest()
        changeRequest.displayName = self.fullNameTxt.text!
        changeRequest.commitChanges(completion: nil)
        let imageRef = FIRStorage.storage().reference().child(Constants.USERS).child("\(uid).jpg")
        
        if self.profilePic.image == nil {
            self.profilePic.image = UIImage(named: "profile-placeholder")
        }
        
        let data = UIImageJPEGRepresentation(self.profilePic.image!, 0.5)
        let uploadTask = imageRef.put(data!, metadata: nil, completion: { (metadata, err) in
            if err != nil {
                print(err!.localizedDescription)
            }
            imageRef.downloadURL(completion: { (url, er) in
                if er != nil {
                    print(er!.localizedDescription)
                }
                
                
                let userInfo: [String : Any] = [Constants.USER_ID : uid,
                                                Constants.FULL_NAME : self.fullNameTxt.text!,
                                                
                                                Constants.PHONE : self.phoneNumTxt.text!,
                                                
                                                Constants.CITY : self.cityTxt.text!,
                                                Constants.PROFILE_PICTURE_URL: url?.absoluteString as Any]
                
                self.ref.child(Constants.USERS).child(uid).updateChildValues(userInfo)
                
                let alert = UIAlertController(title: "Sucessful", message: "Your profile has been updated succesfully ", preferredStyle: .alert)
                
                //let ok = UIAlertAction(title:"OK", style: .default, handler:z nil)
                let ok = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                    
                    
                    let tabvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:  "usersVC") as! UITabBarController
                    
                    let vc = tabvc.viewControllers?[0] as! HomeViewController
                    vc.viewThisUser = FIRAuth.auth()?.currentUser?.uid
                    self.present(tabvc, animated: true, completion: nil)
                    
                })
                alert.addAction(ok)
                self.present(alert, animated: true, completion:nil)
                /*   let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "usersVC")
                 self.present(vc, animated: true, completion: nil)   */
                
                
            })
            
            
        })
        uploadTask.resume()
    }
    
    @IBAction func cancel(_ sender: Any) {
        
        loadUserInfo()
    }
}
