//
//  FriendsShoutViewController.swift
//  Move
//
//  Created by Raj karthik Nalini mohan on 17/2/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//

import UIKit
import Firebase

class FriendsShoutViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    

    @IBOutlet weak var friendsShoutTableView: UITableView!
    @IBOutlet weak var aivLoading: UIActivityIndicatorView!
    var databaseRef = FIRDatabase.database().reference()
    var viewThisUser: String?
    var viewThisUserData: NSDictionary?
    var shouts = [NSDictionary]()
    var comment = [NSDictionary]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.databaseRef.child("users").child(self.viewThisUser!).observeSingleEvent(of: .value) { (snapshot:FIRDataSnapshot) in
            self.viewThisUserData = snapshot.value as? NSDictionary
            self.databaseRef.child("shouts").child(self.viewThisUser!).observe(.childAdded, with: { (snapshot:FIRDataSnapshot) in
                
                self.shouts.append(snapshot.value as! NSDictionary)
                
                
                self.friendsShoutTableView.insertRows(at: [IndexPath(row:0,section:0)], with: UITableViewRowAnimation.automatic)
                
                self.aivLoading.stopAnimating()
                
            }){(error) in
                
                //print(error)
            }
        }
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  
     func numberOfSections(in tableView: UITableView) -> Int {
     return 1
     }
     
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return self.shouts.count
     
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
     
     let cell: FriendsViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FriendsViewTableViewCell", for: indexPath) as! FriendsViewTableViewCell
     
     let currentShout = shouts[(self.shouts.count-1) - (indexPath.row)]
     
     
     cell.configure(self.viewThisUserData!["urlToImage"] as! String,name:self.viewThisUserData!["fullname"] as! String,shout:currentShout["text"] as! String, timestamp: currentShout["timestamp"] as! String)
        
        let counte = currentShout["counter"] as! Int
        var counts = "Comments (\(counte))"
        cell.commentsCount.setTitle(counts, for: .normal)
        cell.tapAction = { (cell) in
            //self.showAlertForRow(row: self.homeTableView.indexPath(for: cell)!.row)
            //move to comment vc
            let shoutThatWasTapped = self.shouts[(self.shouts.count-1) - (self.friendsShoutTableView.indexPath(for: cell)!.row)]
            
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "comments") as! CommentsViewController
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .coverVertical
            vc.shoutid = shoutThatWasTapped["shoutid"] as! String
            vc.shoutUserId = shoutThatWasTapped["userid"] as! String
            vc.isFromHome = false
            
            self.present(vc, animated: true, completion: nil)
            //once completed presents home
            
            // print("view comments")
            
        }

     return cell
     }
    
    @IBAction func backToFriendsShout(segue: UIStoryboardSegue){
        
    }
    
}
