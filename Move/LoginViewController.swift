//
//  loginViewController.swift
//  Move
//
//  Created by Raj karthik Nalini mohan on 30/1/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseStorage

class LoginViewController: UIViewController {
    

    @IBOutlet weak var emailAddr: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /*
         FIRAuth.auth()?.signIn(withEmail: "raj@gmail.com", password: "123456", completion: nil)
         let tabvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:  "usersVC") as! UITabBarController
         
         let vc = tabvc.viewControllers?[0] as! HomeViewController
         vc.viewThisUser = FIRAuth.auth()?.currentUser?.uid
         self.present(tabvc, animated: true, completion: nil)   */
        /* let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "usersVC")
         self.present(vc, animated: true, completion: nil) */
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        
        
        let email = emailAddr.text!.lowercased()
        let  finalEmail = email.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if finalEmail.isEmpty || password.text!.isEmpty {
            print("One or more fields have not been filled. Please try again.")
            let alertController = UIAlertController(title: "Something went wrong", message: "One or more fields have not been filled. Please try again.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "ok", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        } else
        {
            
            guard emailAddr.text != "", password.text != "" else {
                return
            }
            
            FIRAuth.auth()?.signIn(withEmail: emailAddr.text!, password: password.text!, completion: {
                (user, error) in
                
                if error != nil {
                    //error occured
                   print(error?.localizedDescription)
                    
                    let alertController = UIAlertController(title: "Something went wrong", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)

                }else{
                    //present tab bar
                    let tabvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:  "usersVC") as! UITabBarController
                    
                    let vc = tabvc.viewControllers?[0] as! HomeViewController
                    vc.viewThisUser = FIRAuth.auth()?.currentUser?.uid
                    self.present(tabvc, animated: true, completion: nil)
                    //once completed presents home
                    
                }
            })}
        
        
        
        
    }
    
    @IBAction func forgotPasswordPressed(_ sender: Any) {
        
        
        let tabvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:  "forgotPassword") as! UIViewController
        
    
        self.present(tabvc, animated: true, completion: nil)
    }
    
    
    @IBAction func backToLogin(segue:UIStoryboardSegue){
        
    }
    @IBAction func signupToLogin(segue:UIStoryboardSegue){
        
    }
    
}




