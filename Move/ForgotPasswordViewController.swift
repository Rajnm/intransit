//
//  ForgotPasswordViewController.swift
//  Move
//
//  Created by Raj karthik Nalini mohan on 23/2/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//

import UIKit
import  Firebase

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var email: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitPressed(_ sender: Any) {
        
        if self.email.text == ""
        {
            let alertController = UIAlertController(title: "Something went wrong", message: "Please enter an email address", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "ok", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            FIRAuth.auth()?.sendPasswordReset(withEmail: self.email.text!, completion: { (error) in
                var title = ""
                var message = ""
                if error != nil
                {
                    title = "Something went wrong"
                    message = (error?.localizedDescription)!
                }
                    else {
                    title = "success!"
                    message = "password reset email sent."
                    self.email.text = ""
                        
                    }
                let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
                
               
              //  let defaultAction = UIAlertAction(title: "ok", style: .cancel, handler: backToLogin())
                let defaultAction = UIAlertAction(title: "ok", style: .cancel, handler: { (UIAlertAction) in
                    let tabvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:  "login") as! UIViewController
                    
                    self.present(tabvc, animated: true, completion: nil)
                })
                alertController.addAction(defaultAction)
                   self.present(alertController, animated: true, completion: nil)
                
            })
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}
