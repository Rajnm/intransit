//
//  firstShoutViewController.swift
//  Move
//
//  Created by Raj karthik Nalini mohan on 14/2/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//

import UIKit
import Firebase
class firstShoutViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var newShoutTextView: UITextView!
    
    
    var databaseRef = FIRDatabase.database().reference()
    var loggedInUser:AnyObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loggedInUser = FIRAuth.auth()?.currentUser
        newShoutTextView.delegate = self
        self.newShoutTextView.layer.borderWidth = 2.5
        self.newShoutTextView.layer.borderColor = UIColor.gray.cgColor
            self.newShoutTextView.layer.cornerRadius = 10
        self.newShoutTextView.layer.masksToBounds = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    
    
    
    @IBAction func didTapShout(_ sender: Any) {
        if newShoutTextView.text == "" {
            let alertController = UIAlertController(title: "Shout error", message: "Please enter a text in the shout box, example: Hi friends how are you?", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "ok", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }else{
            let key = self.databaseRef.child("shouts").childByAutoId().key
            
            let dateInSeconds = Date().timeIntervalSince1970
            let date = Date(timeIntervalSince1970: dateInSeconds)
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd hh:mm a"
            
            let stamp = "\(formatter.string(from: date))"
            
            let counts = 0
            
            //let shoutid = self.databaseRef.child("shoutid").childByAutoId()
            
            
            let childUpdates = ["/shouts/\(self.loggedInUser!.uid!)/\(key)/text":newShoutTextView.text,
                                "/shouts/\(self.loggedInUser!.uid!)/\(key)/timestamp":stamp,
                                "/shouts/\(self.loggedInUser!.uid!)/\(key)/shoutid":key,
                                "/shouts/\(self.loggedInUser!.uid!)/\(key)/userid":loggedInUser?.uid as Any,
                                "/shouts/\(self.loggedInUser!.uid!)/\(key)/counter":counts] as [String : Any]
            
            self.databaseRef.updateChildValues(childUpdates)
            let tabvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:  "usersVC") as! UITabBarController
            
            let vc = tabvc.viewControllers?[0] as! HomeViewController
            vc.viewThisUser = FIRAuth.auth()?.currentUser?.uid
            self.present(tabvc, animated: true, completion: nil)                
        }
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
}
