//
//  ChatHomeViewController.swift
//  Chat
//
//  Created by Linus on 9/2/17.
//  Copyright © 2017 Move. All rights reserved.
//

import UIKit
import Firebase

class MessagesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var messagesTable: UITableView!
    
    var users = [User]()
    var messages = [Message]()
    var messagesDictionary = [String: Message]()
    var timer: Timer?
    
    let CELL_ID = "userCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //observeMessages()
        
        
        observeUserMessages()
        // Do any additional setup after loading the view.
    }
    
    func observeMessages(){
        guard let uid = FIRAuth.auth()?.currentUser?.uid else{
            return
        }
        
        let allMessagesRef = FIRDatabase.database().reference().child(Constants.MESSAGES)
        allMessagesRef.observe(.childAdded, with: { (snapshot) in
            let messageId = snapshot.key
            let messageRef = allMessagesRef.child(messageId)
            
            messageRef.observeSingleEvent(of: .value, with: { (snapshot2) in
                if let dictionary = snapshot2.value as? [String: Any]{
                    let message = Message()
                    message.setValuesForKeys(dictionary)
                    self.messages.append(message)
                    self.handleReloadTable()
                }
            })
        })
    }
    
    func observeUserMessages(){
        guard let uid = FIRAuth.auth()?.currentUser?.uid else{
            return
        }
        
        let ref = FIRDatabase.database().reference().child(Constants.USER_MESSAGES).child(uid)
        ref.observe(.childAdded, with: { (snapshot) in
            
            let recieverId = snapshot.key
            FIRDatabase.database().reference().child(Constants.USER_MESSAGES).child(uid).child(recieverId).observe(.childAdded, with: { (snapshot2) in
                //print(snapshot2)
                let messageId = snapshot2.key
                self.fetchMessageWith(messageId: messageId)
            })
        })
    }
    
    private func fetchMessageWith(messageId: String){
        let messageRef = FIRDatabase.database().reference().child(Constants.MESSAGES).child(messageId)
        
        messageRef.observeSingleEvent(of: .value, with: { (snapshot3) in
            if let dictionary = snapshot3.value as? [String: Any]{
                var message = Message()
                message.setValuesForKeys(dictionary)
                if let chatPartnerId = message.chatPartnerId(){
                    //contains all of messages, but only one per user
                    self.messagesDictionary[chatPartnerId] = message
                }
                self.attemptReloadOfTable()
            }
        })
        
    }
    
    private func attemptReloadOfTable(){
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.handleReloadTable), userInfo: nil, repeats: false)
    }
    
    func handleReloadTable(){
        self.messages = Array(self.messagesDictionary.values)
        self.messages.sort(by: { (message1, message2) -> Bool in
            return (message1.timestamp?.intValue)! > (message2.timestamp?.intValue)!
        })
        
        //this will crash bc of background thread use async dispatch main thread
        DispatchQueue.main.async {
            self.messagesTable.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = messagesTable.dequeueReusableCell(withIdentifier: CELL_ID, for: indexPath) as? UserCell
        
        let message = messages[indexPath.row]
        if let id = message.chatPartnerId(){
            let userRef = FIRDatabase.database().reference().child(Constants.USERS)
            userRef.child(id).observeSingleEvent(of: .value, with: { (snapshot) in
                if let dictionary = snapshot.value as? [String: Any]{
                    var user = User()
                    user.setValuesForKeys(dictionary)
                    cell?.titleLabel.text = user.fullname
                    cell?.subTitleLabel.text = message.text
                    cell?.timestampLabel.text = cell?.convertTimestamp(seconds: message.timestamp!)
                    
                    if let profileImageUrl = user.urlToImage {
                        cell?.profilePicture?.loadImageUsingCacheWithUrlString(profileImageUrl)
                        cell?.profilePicture?.layer.cornerRadius = 24
                        cell?.profilePicture?.layer.masksToBounds = true
                    }
                }
            })
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //move to chatlog depending on row selected
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Chat", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ChatLog") as! ChatLogViewController
        let msg = messages[indexPath.row]
        vc.recieverId = msg.chatPartnerId()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func handleNewMessage(_ sender: Any) {
        let sb = UIStoryboard(name: "Chat", bundle: nil)
        let vc : NewMessageViewController = sb.instantiateViewController(withIdentifier: "NewMessage") as! NewMessageViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .coverVertical
        vc.messagesViewController = self
        self.present(vc, animated: true, completion: nil)
        
    }
    
    func showChatLogFor(toId: String){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Chat", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ChatLog") as! ChatLogViewController
        vc.recieverId = toId
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func handleBack(segue:UIStoryboardSegue) {
    }
}
