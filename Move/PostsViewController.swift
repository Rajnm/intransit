//
//  TopicViewController.swift
//  Forum
//
//  Created by Linus on 16/2/17.
//  Copyright © 2017 Move. All rights reserved.
//

import UIKit
import Firebase

class PostsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var postsTable: UITableView!
    @IBOutlet weak var postSearchBar: UISearchBar!
    
    var topicId: String?
    var senderDisplayName: String?
    var posts = [Post]()
    var filteredPosts = [Post]()
    var topicTitle: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        postsTable.dataSource = self
        postSearchBar.delegate = self
        title = topicTitle
        observePosts()
        
        postsTable.rowHeight = UITableViewAutomaticDimension
        postsTable.estimatedRowHeight = 140
    }
    
    func observePosts(){
        let postsForTopicRef = FIRDatabase.database().reference().child(Constants.POSTS).child(topicId!)
        postsForTopicRef.observe(.childAdded, with: {(snapshot) in
            var post = Post()
            let dictionary = snapshot.value as! [String: Any]
            
            post.setValuesForKeys(dictionary)
            
            self.posts.append(post)
            self.filteredPosts.append(post)
            
            //this will crash bc of background thread use async dispatch main thread
            DispatchQueue.main.async {
                self.postsTable.reloadData()
            }
        })
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        filteredPosts = searchText.isEmpty ? posts: posts.filter { (item: Post) -> Bool in
            return item.title?.range(of: searchText, options: .caseInsensitive, range: nil, locale:nil) != nil
        }
        
        postsTable.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredPosts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = postsTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PostCell
        
        let post = filteredPosts[indexPath.row]
        
        cell.postTitle?.text = post.title
        cell.postAuthor?.text = post.senderDisplayName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let post = posts[indexPath.row]
        self.performSegue(withIdentifier: "showPost", sender: post)
    }
    
    @IBAction func createNewPostPressed(_ sender: Any) {
        performSegue(withIdentifier: "newPostSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPost" {
            if let post = sender as? Post {
                let vc = segue.destination as! PostViewController
                vc.senderDisplayName = self.senderDisplayName
                vc.hidesBottomBarWhenPushed = true
                vc.post = post
            }
        }
        if segue.identifier == "newPostSegue" {
            let vc = segue.destination as! NewPostViewController
            vc.postsViewController = self
            vc.senderDisplayName = self.senderDisplayName
            vc.topicId = self.topicId
            vc.hidesBottomBarWhenPushed = true
        }
    }
    
    func showPostFor(postId: String){
        //get post from postId
        let postsRef = FIRDatabase.database().reference().child(Constants.POSTS).child(topicId!)
        postsRef.child(postId).observeSingleEvent(of: .value, with: { (snapshot) in
            let dict = snapshot.value as! [String: Any]
            var post = Post()
            post.setValuesForKeys(dict)
            
            self.performSegue(withIdentifier: "showPost", sender: post)
        })
    }
    
    @IBAction func cancelBackToPosts(segue: UIStoryboardSegue){
    }
}
