//
//  CommentsTableViewCell.swift
//  Move
//
//  Created by Raj karthik Nalini mohan on 21/2/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//

import UIKit
import Firebase

class CommentsTableViewCell: UITableViewCell {

    @IBOutlet weak var comments: UILabel!
    @IBOutlet weak var commentsView: UITextView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var fullName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    
    func getUserDataForUserId(id: String){
        //create ref
        
        let userRef = FIRDatabase.database().reference().child(Constants.USERS).child(id)
        let storageRef = FIRStorage.storage()
        //observe the current userid
        userRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String : Any]{
                
                //assign an object this data
                let user = User()
                user.setValuesForKeys(dictionary)
                self.fullName.text = user.fullname
                let imageUrl = user.urlToImage
                storageRef.reference(forURL: imageUrl!).data(withMaxSize: 1 * 1024 * 1024, completion: { (imgData, error) in
                    if error == nil {
                        if let data = imgData {
                            self.profileImage.image = UIImage(data: data)
                        }
                    } else {
                        print(error!.localizedDescription)
                    }
                    
                })
                
            }
        })
    }
    
    
    
    
    
    open func configure(_ comment: String,timestamp: String)
    {
        self.commentsView.text = comment
        
        comments.text = timestamp
        /*
         self.fullName.text = name
        
        let imageData = try? Data(contentsOf: URL(string:profileImage)!)
        self.profileImage.image = UIImage(data:imageData!)
        */
    }
}
