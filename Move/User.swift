//
//  User.swift
//  Chat
//
//  Created by Linus on 9/2/17.
//  Copyright © 2017 Move. All rights reserved.
//


import Foundation
import Firebase

class User: NSObject{
    var uid: String?
    var fullname: String?
    var email: String?
    
    var phonenum: String?
    var dateofbirth: String?
    var gender: String?
    var city: String?
    
    var urlToImage: String?
}
