//
//  ChatLogViewController.swift
//  Chat
//
//  Created by Linus on 9/2/17.
//  Copyright © 2017 Move. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import MobileCoreServices
import AVKit
import Firebase

protocol FetchUserDelegate{
    func didFetchFromUser(user: User)
    func didFetchToUser(user: User)
}

class ChatLogViewController: JSQMessagesViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, FetchUserDelegate {
    
    private var messages = [JSQMessage]()
    let picker = UIImagePickerController()
    var recieverId: String?
    
    var senderProfileUrl: String?
    var recieverProfileUrl: String?
    
    var senderImg: UIImage?
    var recvImg: UIImage?
    //var chatPartnerId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        senderId = ""
        senderDisplayName = ""
        
        getSenderNameAndId()
        
        picker.delegate = self
        // Do any additional setup after loading the view.
        
        observeMessages()
    }
    
    

    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(named:"Player"), diameter: 30)
        
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        
        let bubbleFactory = JSQMessagesBubbleImageFactory()
        
        let message = messages[indexPath.item]
        if message.senderId == self.senderId {
            return bubbleFactory?.outgoingMessagesBubbleImage(with: UIColor.blue)
        } else {
            return bubbleFactory?.incomingMessagesBubbleImage(with: UIColor.lightGray)
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
        let message = messages[indexPath.item]
        
        if message.senderId == self.senderId {
            cell.textView!.textColor = UIColor.white
            cell.avatarImageView.loadImageUsingCacheWithUrlString(senderProfileUrl!)
        } else {
            cell.textView!.textColor = UIColor.black
            cell.avatarImageView.loadImageUsingCacheWithUrlString(recieverProfileUrl!)
        }
        
        cell.avatarImageView.layer.cornerRadius = 15
        cell.avatarImageView.layer.masksToBounds = true
        
        return cell
    }
    
    
    
    // Make space to display time for last sent chat
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
        
        let message = messages[indexPath.item]
        //Sent by me, skip
        if message.senderId == senderId {
            return CGFloat(0.0)
        }
        
        //Same as previous sender, skip
        if indexPath.item > 0 {
            let previousMessage = messages[indexPath.item-1]
            if previousMessage.senderId == message.senderId {
                return CGFloat(0.0)
            }
        }
        return kJSQMessagesCollectionViewCellLabelHeightDefault
    }
    
    // Displaying time stamp in chat.. for previous chats
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let message: JSQMessage = self.messages[indexPath.item]
        
        if message.senderId == senderId {
            return nil
        }
        
        if indexPath.item > 0{
            let prevMessage = messages[indexPath.item-1]
            if prevMessage.senderId == message.senderId {
                return nil
            }
        }
        
        return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
    }
    
    override func didPressAccessoryButton(_ sender: UIButton!) {
        let alert = UIAlertController(title: "Media messages", message: "Please select a media", preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let photos = UIAlertAction(title: "Photos", style: .default, handler: { (alert: UIAlertAction) in
            
            self.chooseMedia(type: kUTTypeImage)
            
        })
        let videos = UIAlertAction(title: "Videos", style: .default, handler:  {(alert: UIAlertAction) in
            self.chooseMedia(type: kUTTypeMovie)
            
        })
        
        alert.addAction(photos)
        alert.addAction(videos)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    //PICKER FOR IMAGES
    private func chooseMedia(type: CFString){
        picker.mediaTypes = [type as String]
        present(picker, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pic = info[UIImagePickerControllerOriginalImage] as? UIImage{
            let data = UIImageJPEGRepresentation(pic, 0.01)
            sendMedia(image: data, video: nil, senderId: senderId, senderName: senderDisplayName)
            
        }else if let vid = info[UIImagePickerControllerMediaURL] as? URL{
            sendMedia(image: nil, video: vid, senderId: senderId, senderName: senderDisplayName)
        }
        self.dismiss(animated: true, completion: nil)
        collectionView.reloadData()
    }
    
    func sendMedia(image: Data?, video: URL?, senderId: String, senderName: String){
        let mediaMsgRef = FIRDatabase.database().reference().child(Constants.MEDIA_MESSAGES)
        if (image != nil) {
            let imgStoreRef = FIRStorage.storage().reference().child(Constants.IMAGE_STORAGE)
            //store image into firebase storage
            imgStoreRef.child(senderId + "\(NSUUID().uuidString).jpg").put(image!, metadata: nil) {
                (metadata: FIRStorageMetadata?, err: Error?) in
                if err != nil {
                    //inform user there was a problem uploading the image
                    print("Problem uploading image")
                }else{
                    //image successfully uploaded to storage, post msg in chat
                    let data: Dictionary<String, Any> = [Constants.FROM_ID: self.senderId,
                                                         Constants.TO_ID: self.recieverId!,
                                                         Constants.URL: String (describing: metadata!.downloadURL()!),
                                                         Constants.TIMESTAMP: NSNumber(value: Int(Date().timeIntervalSince1970))]
                    
                    mediaMsgRef.childByAutoId().setValue(data)
                }
            }
            
        } else if (video != nil) {
            let vidStoreRef = FIRStorage.storage().reference().child(Constants.VIDEO_STORAGE)
            //Store video into firebase storage
            vidStoreRef.child(senderId + "\(NSUUID().uuidString).jpg").putFile(video!, metadata:nil){
                (metadata: FIRStorageMetadata?, err: Error?) in
                if err != nil{
                    //inform user that upload video has failed using delegation
                }else{
                    let data: Dictionary<String, Any> = [Constants.FROM_ID: self.senderId,
                                                         Constants.TO_ID: self.recieverId!,
                                                         Constants.URL: video!.absoluteString,
                                                         Constants.TIMESTAMP: NSNumber(value: Int(Date().timeIntervalSince1970))]
                    
                    mediaMsgRef.childByAutoId().setValue(data)
                }
                
            }
        }
    }
    
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!){
        let msg = messages[indexPath.item]
        if msg.isMediaMessage {
            if let mediaItem = msg.media as? JSQVideoMediaItem{
                let player = AVPlayer(url: mediaItem.fileURL)
                let playerController = AVPlayerViewController();
                playerController.player = player
                self.present(playerController, animated: true, completion: nil)
            }
        }
        
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        //send message and save to database
        
        self.finishSendingMessage()
        
        let timestamp = NSNumber(value: Int(date.timeIntervalSince1970))
        let values: Dictionary<String, Any> = [Constants.FROM_ID: senderId,
                                               Constants.TEXT: text,
                                               Constants.TO_ID: recieverId,
                                               Constants.TIMESTAMP: timestamp]
        
        let textMsgRef = FIRDatabase.database().reference().child(Constants.MESSAGES)
        let childRef = textMsgRef.childByAutoId()
        //childRef.setValue(values)
        
        childRef.updateChildValues(values) { (error, ref) in
            if error != nil{
                print(error!.localizedDescription)
                return
            }else{
                let userMessagesRef = FIRDatabase.database().reference().child(Constants.USER_MESSAGES).child(senderId).child(self.recieverId!)
                let messageId = childRef.key
                userMessagesRef.updateChildValues([messageId: 1])
                
                let recipientUserMessagesRef = FIRDatabase.database().reference().child(Constants.USER_MESSAGES).child(self.recieverId!).child(senderId)
                recipientUserMessagesRef.updateChildValues([messageId: 1])
                
                
                //save msg to database with reference to chat partners
                
                self.collectionView.reloadData()
                
                //remove text from textfield
                
            }
        }
    }
    
    func observeMessages(){
        guard let uid = FIRAuth.auth()?.currentUser?.uid, let recvId = recieverId else{
            return
        }
        let userMessagesRef = FIRDatabase.database().reference().child(Constants.USER_MESSAGES).child(uid).child(recvId)
        userMessagesRef.observe(.childAdded, with: { (snapshot) in
            //print(snapshot)
            let messageId = snapshot.key
            let messagesRef = FIRDatabase.database().reference().child(Constants.MESSAGES).child(messageId)
            
            messagesRef.observeSingleEvent(of: .value, with: { (snapshot2) in
                guard let dictionary = snapshot2.value as? [String: Any] else {
                    return
                }
                
                let message = Message()
                message.setValuesForKeys(dictionary)
                //normally would append message and reload view
                //get name of message sender and append a new message
                let usersRef = FIRDatabase.database().reference().child(Constants.USERS)
                usersRef.child(message.fromId!).observeSingleEvent(of: .value, with: { (snapshot3) in
                    let dictionary = snapshot3.value as! [String: Any]
                    var user = User()
                    user.setValuesForKeys(dictionary)
                    let timestampDate = Date(timeIntervalSince1970: (message.timestamp?.doubleValue)!)
                    let jsqMsg = JSQMessage(senderId: user.uid, senderDisplayName: user.fullname, date: timestampDate, text: message.text)
                    self.messages.append(jsqMsg!)
                    
                    DispatchQueue.main.async {
                        self.messages.sort(by: { (m1, m2) -> Bool in
                            return m1.date < m2.date
                        })
                        self.collectionView?.reloadData()
                        self.finishReceivingMessage()
                    }
                })
            })
        })
    }
    
    func getSenderNameAndId(){
        guard let fromId =  FIRAuth.auth()?.currentUser?.uid, let toId = recieverId else{
            return
        }
        let usersRef = FIRDatabase.database().reference().child(Constants.USERS)
        usersRef.child(fromId).observeSingleEvent(of: .value, with: { (snapshot) in
            var fromUser = User()
            if let dictionary = snapshot.value as? [String: Any]{
                fromUser.setValuesForKeys(dictionary)
            }
            self.didFetchFromUser(user: fromUser)
        })
        
        usersRef.child(toId).observeSingleEvent(of: .value, with: { (snapshot) in
            var toUser = User()
            if let dictionary = snapshot.value as? [String: Any]{
                toUser.setValuesForKeys(dictionary)
            }
            self.didFetchToUser(user: toUser)
        })
        
    }
    
    func didFetchFromUser(user: User) {
        self.senderId = user.uid
        self.senderDisplayName = user.fullname
        self.senderProfileUrl = user.urlToImage
    }
    
    func didFetchToUser(user: User) {
        self.navigationItem.title = user.fullname
        self.recieverProfileUrl = user.urlToImage
    }
}
