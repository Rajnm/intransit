//
//  FriendsViewTableViewController.swift
//  Move
//
//  Created by Raj karthik Nalini mohan on 16/2/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//

import UIKit
import Firebase

class FriendsViewTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating {
    
    @available(iOS 8.0, *)
    public func updateSearchResults(for searchController: UISearchController) {
        filterContent(searchText: self.searchController.searchBar.text!)
        
    }

    
    @IBOutlet weak var followUserTableView: UITableView!


    let searchController = UISearchController(searchResultsController: nil)
    var userArray = [NSDictionary?]()
    var filteredUsers = [NSDictionary?]()
    var databaseRef = FIRDatabase.database().reference()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        
        self.followUserTableView.tableHeaderView = searchController.searchBar
        
        databaseRef.child("users").queryOrdered(byChild: "fullname").observe(.childAdded, with: { (snapshot) in
            self.userArray.append(snapshot.value as? NSDictionary)
            
         
/*
 let imageUrl = users.urlToImage
 self.storageRef.reference(forURL: imageUrl!).data(withMaxSize: 1 * 1024 * 1024, completion: { (imgData, error) in
 if error == nil {
 if let data = imgData {
 self.profilePic.image = UIImage(data: data)
 }
 }
 */
 
            self.followUserTableView.insertRows(at: [IndexPath(row:self.userArray.count-1,section:0)], with: UITableViewRowAnimation.automatic)
            }) { (error) in
            print(error.localizedDescription)
        }
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Table view data source
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if searchController.isActive && searchController.searchBar.text != ""{
            return filteredUsers.count
        }
        return self.userArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let user : NSDictionary?
        
        if searchController.isActive && searchController.searchBar.text != ""{
            user = filteredUsers[indexPath.row]
        }
        else
        {
            user = self.userArray[indexPath.row]
        }
        cell.textLabel?.text = user?["fullname"] as? String
        return cell
    }
    
    func filterContent(searchText:String){
        self.filteredUsers = self.userArray.filter { user in
            let username = user!["fullname"] as? String
            return(username?.lowercased().contains(searchText.lowercased()))!
            
        }
        self.followUserTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let clickedUser = userArray[indexPath.row]
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:  "friendsShout") as! FriendsShoutViewController
        
        
        vc.viewThisUser = clickedUser?["uid"] as! String?

        self.present(vc, animated: true, completion: nil)
       
    }
    
    @IBAction func backToFriends(segue: UIStoryboardSegue){
    
    }
    

}


