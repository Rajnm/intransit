//
//  SignupViewController.swift
//  Move
//
//  Created by Raj karthik Nalini mohan on 30/1/17.
//  Copyright © 2017 Raj karthik Nalini mohan. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseStorage

class SignupViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate,  UITextFieldDelegate {
    
    
    
    
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var emailAddress: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var datePickerTxt: UITextField!
    @IBOutlet weak var signin: UIButton!
    @IBOutlet weak var genTxt: UITextField!
    
    @IBOutlet weak var fullName: UITextField!
    
    @IBOutlet weak var reenterPwd: UITextField!
    @IBOutlet weak var phoneNo: UITextField!
    
    let picker = UIImagePickerController()
    var userStorage:FIRStorageReference!
    var ref:FIRDatabaseReference!
    let datePicker = UIDatePicker()
    
    let genderArray = ["", "Male" ,"Female"]
    let pickker = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        pickker.delegate = self
        pickker.dataSource = self
        genTxt.inputView = pickker
        
        let storage = FIRStorage.storage().reference()
        
        ref = FIRDatabase.database().reference()
        userStorage = storage.child(Constants.USERS)
        createDatePicker()
        let TapGesture = UITapGestureRecognizer(target: self, action: "ImageTapped")
        self.imageView.addGestureRecognizer(TapGesture)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    func createDatePicker() {
        datePicker.datePickerMode = .date
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([doneButton], animated: false)
        datePickerTxt.inputAccessoryView = toolbar
        datePickerTxt.inputView = datePicker
    }
    
    func donePressed() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        datePickerTxt.text = dateFormatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        genTxt.text = genderArray[row]
        self.view.endEditing(false)
    }
    
    @IBAction func selectImagePressed(_ sender: Any) {
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        present(picker, animated: true, completion: nil)
        
    }
    
    func ImageTapped() {
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.imageView.image = image
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func signinPressed(_ sender: Any) {
        /* let email = emailAddress.text!.lowercased()
         let finalEmail = email.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
         */
        
        guard fullName.text != "", emailAddress.text != "", phoneNo.text != "", datePickerTxt.text != "", genTxt.text != "", city.text != "", password.text != "", reenterPwd.text != "" else {
            
            let alertController = UIAlertController(title: "Field error", message: "One or more mandatory fields have not been filled. Please try again.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "ok", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            return
        }
        if password.text == reenterPwd.text {
            FIRAuth.auth()?.createUser(withEmail: emailAddress.text!, password: password.text!, completion: { (user, error) in
                if error != nil {
                    self.alertTheUser(title: "Create User Error", message: (error?.localizedDescription)!)
                }else{
                    //user signed in successfully
                    guard let uid = FIRAuth.auth()?.currentUser?.uid else{
                        return
                    }
                    
                    let changeRequest = FIRAuth.auth()!.currentUser!.profileChangeRequest()
                    changeRequest.displayName = self.fullName.text!
                    changeRequest.commitChanges(completion: nil)
                    
                    let imageRef = FIRStorage.storage().reference().child(Constants.USERS).child("\(uid).jpg")
                    
                    if self.imageView.image == nil {
                        self.imageView.image = UIImage(named: "profile-placeholder")
                    }
                    
                    let data = UIImageJPEGRepresentation(self.imageView.image!, 0.5)
                    
                    let uploadTask = imageRef.put(data!, metadata: nil, completion: { (metadata, err) in
                        if err != nil{
                            
                            let alertController = UIAlertController(title: "Upload image error", message: err?.localizedDescription, preferredStyle: .alert)
                            
                            let defaultAction = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                            alertController.addAction(defaultAction)
                            
                            self.present(alertController, animated: true, completion: nil)
                        }else{
                            //upload image successful
                            //download file from storage and put into database
                            imageRef.downloadURL(completion: { (url, er) in
                                if er != nil {
                                    
                                    let alertController = UIAlertController(title: "Error downloading Image", message: er?.localizedDescription, preferredStyle: .alert)
                                    
                                    let defaultAction = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                                    alertController.addAction(defaultAction)
                                    
                                    self.present(alertController, animated: true, completion: nil)
                                } else {
                                    //downloading of URL is successful, add to database
                                    //get user info from form and put into database
                                    let userInfo: [String : Any] = [Constants.USER_ID : uid,
                                                                    Constants.FULL_NAME : self.fullName.text!,
                                                                    Constants.EMAIL : self.emailAddress.text!,
                                                                    Constants.PHONE : self.phoneNo.text!,
                                                                    Constants.DOB: self.datePickerTxt.text!,
                                                                    Constants.GENDER: self.genTxt.text!,
                                                                    Constants.CITY : self.city.text!,
                                                                    Constants.PROFILE_PICTURE_URL: url?.absoluteString as Any]
                                    
                                    self.ref.child(Constants.USERS).child(uid).setValue(userInfo)
                                    
                                    
                                    let alertController = UIAlertController(title: "Congrats", message: "Registration successful", preferredStyle: .alert)
                                    
                                    //let defaultAction = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                                    let defaultAction = UIAlertAction(title: "ok", style: .cancel, handler: { (UIAlertAction) in
                                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "firstShout") as! firstShoutViewController
                                        
                                        self.present(vc, animated: true, completion: nil)
                                    })
                                    alertController.addAction(defaultAction)
                                    
                                    self.present(alertController, animated: true, completion: nil)
                                }
                            })
                        }
                    })
                    uploadTask.resume()
                    
                    
                   
                }
                
            })
            
        } else {
            self.alertTheUser(title: "Password error", message: "Password does not match, please re-enter")
        }
    }
    
}

