//
//  Reply.swift
//  Forum
//
//  Created by Linus on 16/2/17.
//  Copyright © 2017 Move. All rights reserved.
//

import Foundation

class Reply: NSObject{
    var senderId: String?
    var senderDisplayName: String?
    var text: String?
    var timestamp: NSNumber?
}


